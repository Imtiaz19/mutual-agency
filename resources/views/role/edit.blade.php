@extends('layouts.master')
@section('title', 'Create Permission')
@section('con')
    @include('flash::message')
    @include('layouts.error')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title">Edit Role</h3></div>
        <div class="box-body">
            <a href="{{route('role.index')}}" class="btn btn-primary"> Back</a>
            <form action="{{route('role.update',$role->id)}}" method="post">
                {{csrf_field()}}
                {{method_field('PATCH')}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3 text-right control-label col-form-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" value="{{$role->name}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Display" class="col-sm-3 text-right control-label col-form-label">Display Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="display_name" class="form-control" value="{{$role->display_name}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description"
                           class="col-sm-3 text-right control-label col-form-label">Description</label>
                    <div class="col-sm-9">
                        <input type="text" name="description" class="form-control" value="{{$role->description}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="permission" class="col-sm-3 text-right control-label col-form-label">Permission</label>
                    <div class="col-sm-9">
                        @foreach($permissions as $permission)
                            <input type="checkbox"
                                   {{in_array($permission->id,$role_permission)?"checked":""}} name="permission[]"
                                   value="{{$permission->id}}"> {{$permission->name}} <br>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection