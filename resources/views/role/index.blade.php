@extends('layouts.master')
@section('title', 'All roles')
@section('con')
    @include('flash::message')
    <div class="create" style="margin-bottom: 20px">
        <a class="btn btn-success " href="{{route('role.create')}}">Create Role</a>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"> <b>Roles</b> </h3>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @forelse($roles as $role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>{{$role->display_name}}</td>
                        <td>{{$role->description}}</td>
                        <td>
                            <a class="btn btn-raised btn-primary btn-sm" href="{{ route('role.edit',$role->id) }}"><i
                                        class="fa fa-edit" aria-hidden="true"></i></a>
                            <form action="{{route('role.destroy',$role->id)}}" id="delete-form-{{ $role->id }}"
                                  method="post" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                            </form>
                            <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $role->id }}').submit();
                                    }else{
                                    event.preventDefault();
                                    }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No Roles</td>
                    </tr>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
@endsection