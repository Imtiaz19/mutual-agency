@extends('layouts.master')
@section('title', 'Investments')
@section('con')
    @include('layouts.error')
    @include('flash::message')
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><b>Investments</b> </h3>
        </div>

        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>User Id</th>
                    <th>User Type</th>
                    <th>Investment Type</th>
                    <th>Monthly Payment</th>
                    <th>Interest Type</th>
                    <th>Percentage Rate</th>
                    <th>Issue Date</th>
                    <th>Target Date To Collect</th>
                    <th>Payment Due</th>
                    <th>Last Payment Installment</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @forelse($investments as $investment)
                    <tr>
                        <td>{{ $investment->user_id }}</td>
                        <td>{{ $investment->user_type }}</td>
                        <td>{{ $investment->investment_type }}</td>
                        <td>{{ $investment->monthly_payment }}</td>
                        <td>{{ $investment->interest_type}}</td>
                        <td>{{ $investment->percentage_rate}}</td>
                        <td>{{ $investment->issue_date}}</td>
                        <td>{{ $investment->target_date_to_collect}}</td>
                        <td>{{ $investment->payment_due}}</td>
                        <td>{{ $investment->last_payment_installment}}</td>
                        <td>
                            <a class="btn btn-raised btn-primary btn-sm"
                               href="{{ route('investment.edit',$investment->id) }}"><i class="fa fa-edit"
                                                                                  aria-hidden="true"></i></a>
                            <form action="{{route('investment.destroy',$investment->id)}}" id="delete-form-{{ $investment->id }}"
                                  method="post" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                            </form>
                            <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $investment->id }}').submit();
                                    }else{
                                    event.preventDefault();
                                    }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No Data</td>
                    </tr>
                @endforelse
                </tbody>

            </table>
        </div>
    </div>
@endsection