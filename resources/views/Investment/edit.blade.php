@extends('layouts.master')
@section('title', 'Investment')
@section('con')
    @include('layouts.error')
    @include('flash::message')

    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"> <b>Investment</b> </h3></div>
        <div class="box-body">
            <form action="{{ route('investment.update',$investments->id)}}" method="post">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3 text-right control-label col-form-label">Members Name</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="member">
                            @foreach($users as $user)
                                <option  value="{{$user->id}}" @if ($investments->user_id == $user->id) selected @endif > {{ $user->first_name.' '.$user->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ut" class="col-sm-3 text-right control-label col-form-label">Users Type</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="user_type">
                        <option hidden disabled selected value> Select User type</option>
                        <option value="member" @if ($investments->user_type == "member")selected="selected" @endif>Members</option>
                        <option value="others" @if ($investments->user_type == "others")selected="selected" @endif>Others</option>
                     
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="it" class="col-sm-3 text-right control-label col-form-label">Investment Type</label>
                    <div class="col-sm-3">
                        <input type="text" name="investment_type" value="{{ $investments->investment_type }}" class="form-control" id="it">
                    </div>
                </div>
                <div class="form-group row"> 
                    <label for="mp" class="col-sm-3 text-right control-label col-form-label">Monthly Payment</label>
                    <div class="col-sm-3">
                        <input type="text" name="monthly_payment"  value="{{ $investments->monthly_payment }}" class="form-control" id="mp">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="int" class="col-sm-3 text-right control-label col-form-label">Interest Type</label>
                    <div class="col-sm-3">
                       <select class="form-control" name="interest_type">
                        <option value="percentage" @if ($investments->interest_type == "percentage")selected="selected" @endif>Percentage</option>
                        <option value="fixed" @if ($investments->interest_type == "fixed")selected="selected" @endif>Fixed</option>
                    </select>
                    </div>
                </div>
                <div class="form-group row"> 
                    <label for="pr" class="col-sm-3 text-right control-label col-form-label">Percentage Rate</label>
                    <div class="col-sm-3">
                        <input type="text" name="percentage_rate" value="{{ $investments->percentage_rate }}" class="form-control" id="pr">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="datepicker" class="col-sm-3 text-right control-label col-form-label">Issue Date</label>
                    <div class="col-sm-3">
                        <input type="text" name="issue_date" value="{{ $investments->issue_date }}" class="form-control" id="datepicker">
                    </div>
                </div>
                <div class="form-group row"> 
                    <label for="targetdate" class="col-sm-3 text-right control-label col-form-label">Target Date To Collect</label>
                    <div class="col-sm-3">
                        <input type="text" name="target_date_to_collect" value="{{ $investments->target_date_to_collect }}" class="form-control" id="targetdate">
                    </div>
                </div>
                <div class="form-group row"> 
                    <label for="pd" class="col-sm-3 text-right control-label col-form-label">Payment Due</label>
                    <div class="col-sm-3">
                        <input type="text" name="payment_due" value="{{ $investments->payment_due }}"  class="form-control" id="pd">
                    </div>
                </div>
                <div class="form-group row"> 
                    <label for="lastdate" class="col-sm-3 text-right control-label col-form-label">Last Payment Installment</label>
                    <div class="col-sm-3">
                        <input type="text" name="last_payment_installment" value="{{ $investments->last_payment_installment }}" class="form-control" id="lastdate">
                    </div>
                </div>
                <div class="border-top">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    @endsection


