@extends('layouts.master')
@section('title', 'Create Permission')
@section('con')
    @include('layouts.error')
    @include('flash::message')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"> <b>Create Permission</b> </h3></div>
        <div class="box-body">
            <form action="{{ route('permission.store')}}" method="post">
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-sm-3 text-right control-label col-form-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Permission Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Display" class="col-sm-3 text-right control-label col-form-label">Display Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="display" class="form-control" id="display" placeholder="Display name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description"
                           class="col-sm-3 text-right control-label col-form-label">Description</label>
                    <div class="col-sm-9">
                        <input type="text" name="description" class="form-control" id="description"
                               placeholder="Description">
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>

        </div>
    </div>
@endsection
