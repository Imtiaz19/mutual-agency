@extends('layouts.master')
@section('title', 'Create Permission')
@section('con')
    @include('flash::message')
    <div class="create" style="margin-bottom: 50px">
        <a class="btn btn-success " href="{{route('permission.create')}}">Create Permission</a>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"> <b>Permissions</b> </h3>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @forelse($permissions as $permission)
                    <tr>
                        <td>{{$permission->name}}</td>
                        <td>{{$permission->display_name}}</td>
                        <td>{{$permission->description}}</td>
                        <td>
                            <a class="btn btn-raised btn-primary btn-sm"
                               href="{{ route('permission.edit',$permission->id) }}"><i class="fa fa-edit"
                                                                                        aria-hidden="true"></i></a>
                            <form action="{{route('permission.destroy',$permission->id)}}"
                                  id="delete-form-{{ $permission->id }}" method="post" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                            </form>
                            <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $permission->id }}').submit();
                                    }else{
                                    event.preventDefault();
                                    }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No Permission</td>
                    </tr>
            @endforelse
                </tbody>
        </table>

        </div>
    </div>


    



    </div>
@endsection