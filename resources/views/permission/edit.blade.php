@extends('layouts.master')
@section('title', 'Registered User')
@section('con')
    @include('flash::message')
    @include('layouts.error')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title">Edit Permission</h3></div>
        <div class="box-body">
            <a href="{{route('permission.index')}}" class="btn btn-primary"> Back</a>
            <form class="form-horizontal" action="{{ route('permission.update',$permissions->id)}}" method="post">
                {{csrf_field()}}
                {{method_field('PATCH')}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3 control-label ">Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" value="{{$permissions->name}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Display" class="col-sm-3 control-label ">Display Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="display" class="form-control" value="{{$permissions->display_name}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-3  control-label ">Description</label>
                    <div class="col-sm-9">
                        <input type="text" name="description" class="form-control"
                               value="{{$permissions->description}}">
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection