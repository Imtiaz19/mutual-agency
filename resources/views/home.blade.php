@extends('layouts.master')
@section('title', 'Home page')
@section('con')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box-body">
                    <h1>Welcome {{ Auth::user()->first_name.' '.Auth::user()->last_name}}  </h1>
                </div>
            </div>
        </div>
    </div>
@endsection
