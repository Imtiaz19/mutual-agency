@extends('layouts.master')
@section('title', 'Loan Details')
@section('con')

    @include('layouts.error')
    @include('flash::message')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"> <b>Loan Details</b> </h3></div>
        <div class="box-body">
           
                <div class="row">
                <div class="form-group col-md-4">
                    <label for="name" class="col-sm-5  control-label col-form-label">Full Name</label>
                    <div class="col-sm-7">
                            <p class="col-sm-5 ">{{$loans->full_name}}</p>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="address" class="col-sm-5  control-label col-form-label">Address</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->address}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="email" class="col-sm-5  control-label col-form-label">Email</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->email}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="mobile" class="col-sm-5  control-label col-form-label">Mobile</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->mobile}}</p>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="document_collected" class="col-sm-5  control-label col-form-label">Document Collected</label>
                    <div class="col-sm-7">
                            <a href="/files/{{ $loans->document_collected }}">{{ $loans->document_collected }}</a>                           
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="loaner_photo" class="col-sm-5  control-label col-form-label">Loaner Photo</label>
                    <div class="col-sm-7">
                        <img src="/images/{{ $loans->loaner_photo }}" width="100px" height="100px">
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="guarantor_name" class="col-sm-5  control-label col-form-label">Guarantor Name</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->guarantor_name}}</p>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="guarantor_phone" class="col-sm-5  control-label col-form-label">Guarantor Phone</label>
                    <div class="col-sm-7">
                         <p  class="col-sm-5 ">{{$loans->guarantor_phone}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="guarantor_photo" class="col-sm-5  control-label col-form-label">Guarantor Photo</label>
                    <div class="col-sm-7">
                         <img src="/images/{{ $loans->guarantor_photo }}" width="100px" height="100px">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="guarantor_email" class="col-sm-5  control-label col-form-label">Guarantor Email</label>
                    <div class="col-sm-7">
                        <p  class="col-sm-5 ">{{$loans->guarantor_email}}</p>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="document" class="col-sm-5  control-label col-form-label">document</label>
                    <div class="col-sm-7">
                            <a href="/files/{{ $loans->document }}">{{ $loans->document }}</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="loan_amount" class="col-sm-5  control-label col-form-label">Loan Amount</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->loan_amount}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="loan_due" class="col-sm-5  control-label col-form-label">Loan Due</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->loan_due}}</p>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="last_loan_installment" class="col-sm-5  control-label col-form-label">Last Loan Installment</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->last_loan_installment}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="issue_date" class="col-sm-5  control-label col-form-label">Issue Date</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->issue_date}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="approved_by" class="col-sm-5  control-label col-form-label">Approved by</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->approved_by}}</p>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="approved_date" class="col-sm-5  control-label col-form-label">Approved Date</label>
                    <div class="col-sm-7">
                            <p  class="col-sm-5 ">{{$loans->approved_date}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="approved_proof" class="col-sm-5  control-label col-form-label">Approved Proof</label>
                    <div class="col-sm-7">
                            <a href="/files/{{ $loans->approved_proof }}">{{ $loans->approved_proof }}</a>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <label for="last_full_payment_date" class="col-sm-5  control-label col-form-label">Last Full Payment Date</label>
                <div class="col-sm-7">
                        <p  class="col-sm-5 ">{{$loans->last_full_payment_date}}</p>
                </div>
            </div>
        </div>
        
</div>
</div>
@endsection
