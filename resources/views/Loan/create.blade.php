@extends('layouts.master')
@section('title', 'Create Loan')
@section('con')

    @include('layouts.error')
    @include('flash::message')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"> <b>Create Loan</b> </h3></div>
        <div class="box-body">
            <form action="{{ route('loan.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="form-group col-md-4">
                    <label for="name" class="col-sm-5  control-label col-form-label">Full Name</label>
                    <div class="col-sm-7">
                        <input type="text" name="full_name" class="form-control" value="{{ old('full_name') }}" id="name" placeholder="Full Name">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="address" class="col-sm-5  control-label col-form-label">Address</label>
                    <div class="col-sm-7">
                        <input type="text" name="address" class="form-control" value="{{ old('address') }}"  id="address" placeholder="Address">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="email" class="col-sm-5  control-label col-form-label">Email</label>
                    <div class="col-sm-7">
                        <input type="text" name="email" class="form-control" value="{{ old('email') }}" id="email" placeholder="Email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="mobile" class="col-sm-5  control-label col-form-label">Mobile</label>
                    <div class="col-sm-7">
                        <input type="text" name="mobile" value="{{ old('mobile') }}" class="form-control" id="mobile" placeholder="Mobile">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="document_collected" class="col-sm-5  control-label col-form-label">Document Collected</label>
                    <div class="col-sm-7">
                        <input type="file" name="document_collected" value="{{ old('document_collected') }}" class="form-control" id="document_collected">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="loaner_photo" class="col-sm-5  control-label col-form-label">Loaner Photo</label>
                    <div class="col-sm-7">
                        <input type="file" name="loaner_photo" value="{{ old('loaner_photo') }}" class="form-control" id="loaner_photo" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="guarantor_name" class="col-sm-5  control-label col-form-label">Guarantor Name</label>
                    <div class="col-sm-7">
                        <input type="text" name="guarantor_name" value="{{ old('guarantor_name') }}" class="form-control" id="guarantor_name" placeholder="Guarantor Name">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="guarantor_phone" class="col-sm-5  control-label col-form-label">Guarantor Phone</label>
                    <div class="col-sm-7">
                        <input type="text" name="guarantor_phone" value="{{ old('guarantor_phone') }}" class="form-control" id="guarantor_phone" placeholder="Guarantor Phone">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="guarantor_photo" class="col-sm-5  control-label col-form-label">Guarantor Photo</label>
                    <div class="col-sm-7">
                        <input type="file" name="guarantor_photo" value="{{ old('guarantor_photo') }}" class="form-control" id="guarantor_photo" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="guarantor_email" class="col-sm-5  control-label col-form-label">Guarantor Email</label>
                    <div class="col-sm-7">
                        <input type="text" name="guarantor_email" value="{{ old('guarantor_email') }}" class="form-control" id="guarantor_email" placeholder="Guarantor Email ">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="document" class="col-sm-5  control-label col-form-label">document</label>
                    <div class="col-sm-7">
                        <input type="file" name="document" value="{{ old('document') }}" class="form-control" id="document">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="loan_amount" class="col-sm-5  control-label col-form-label">Loan Amount</label>
                    <div class="col-sm-7">
                        <input type="text" name="loan_amount" value="{{ old('loan_amount') }}" class="form-control" id="loan_amount" placeholder="Loan Amount">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="loan_due" class="col-sm-5  control-label col-form-label">Loan Due</label>
                    <div class="col-sm-7">
                        <input type="text" name="loan_due"value="{{ old('loan_due') }}" class="form-control" id="loan_due" placeholder="Loan Due ">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="last_loan_installment" class="col-sm-5  control-label col-form-label">Last Loan Installment</label>
                    <div class="col-sm-7">
                        <input type="text" name="last_loan_installment"value="{{ old('last_loan_installment') }}" class="form-control" id="last_loan_installment" placeholder="last loan installment ">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="issue_date" class="col-sm-5  control-label col-form-label">Issue Date</label>
                    <div class="col-sm-7">
                        <input type="text" name="issue_date"value="{{ old('issue_date') }}" class="form-control" id="datepicker" placeholder="Issue Date">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="approved_by" class="col-sm-5  control-label col-form-label">Approved by</label>
                    <div class="col-sm-7">
                        <input type="hidden" name="approved_by"value="{{ old('approved_by') }}" class="form-control" id="approved_by" >
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="approved_date" class="col-sm-5  control-label col-form-label">Approved Date</label>
                    <div class="col-sm-7">
                        <input type="text" name="approved_date"value="{{ old('approved_date') }}" class="form-control" id="targetdate">
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="approved_proof" class="col-sm-5  control-label col-form-label">Approved Proof</label>
                    <div class="col-sm-7">
                        <input type="file" name="approved_proof"value="{{ old('approved_proof') }}" class="form-control" id="approved_proof" placeholder="approved_proof ">
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <label for="last_full_payment_date" class="col-sm-5  control-label col-form-label">Last Full Payment Date</label>
                <div class="col-sm-7">
                    <input type="text" name="last_full_payment_date"value="{{ old('last_full_payment_date') }}" class="form-control" id="lastdate">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
        <div class="col-sm-6 ">
            <button type="submit" class="btn btn-primary col-sm-3 ">Create</button>
        </div>
    </div>
            
                


            </form>

        </div>
    </div>
@endsection
