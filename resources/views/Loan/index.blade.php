@extends('layouts.master')
@section('title', 'Members Loan ')
@section('con')

    @include('layouts.error')
    @include('flash::message')
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><b>Loans</b> </h3>
            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">

                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th scope="col">No</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Loaner Photo</th>
                    <th>Loan Amount</th>
                    <th>Loan Due</th>
                    <th>Last loan installment</th>
                    <th>Last loan installment</th>
                    <th>Action</th>
                </tr>
                <tbody>
                        <?php $count = 1; ?>
                @forelse($loans as $loan)
                    <tr>                      
                        <th scope="row">{{ $count++ }}</th>
                        <td>{{ $loan->full_name }}</td>
                        <td>{{ $loan->address }}</td>
                        <td>{{ $loan->email }}</td>
                        <td>{{ $loan->mobile }}</td>
                        <td> <img src="/images/{{ $loan->loaner_photo }}" width="70px" height="70px" alt="LP" ></td>             
                        <td>{{ $loan->loan_amount}}</td>
                        <td>{{ $loan->loan_due}}</td>
                        <td>{{ $loan->last_loan_installment}}</td>
                        <td>{{ $loan->last_full_payment_date}}</td> 
                        <td>
                            <a class="btn btn-raised btn-primary btn-sm"
                               href="{{ route('loan.show',$loan->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                               <a class="btn btn-raised btn-primary btn-sm"
                               href="{{ route('loan.edit',$loan->id) }}"><i class="fa fa-edit" aria-hidden="true"></i></a>
                               <form action="{{route('loan.destroy',$loan->id)}}" id="delete-form-{{ $loan->id }}"
                                  method="post" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                            </form>
                            <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $loan->id }}').submit();
                                    }else{
                                    event.preventDefault();
                                    }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection