@extends('layouts.master')
@section('title', 'Registered User')
@section('con')
    @include('layouts.error')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title">Monthly Premium</h3></div>
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('premium.update',$premiums->id)}}" method="post">

                {{csrf_field()}}
                {{method_field('PATCH')}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3 text-right control-label col-form-label">Members Name</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="member">
                            @foreach($users as $user)
                            <option  value="{{$user->id}}" @if ($premiums->member_id == $user->id) selected @endif > {{ $user->first_name.' '.$user->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="month" class="col-sm-3 text-right control-label col-form-label">Month of Payment</label>
                    <div class="col-sm-6">
                        <div id="sla-data-range" class="mrp-container nav navbar-nav">
                            <span class="mrp-icon"><i class="fa fa-calendar"></i></span>
                            <div class="mrp-monthdisplay" name="mop">
                                <span class="mrp-lowerMonth" id="mrp">Jul 2018</span>
                                <span class="mrp-to"> to </span>
                                <span class="mrp-upperMonth">Aug 2014</span>
                            </div>

                            <input type="hidden" value="201408" id="mrp-upperDate"/>
                        </div>
                        <input type="hidden" id="mrp-lowerDate"/>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="amount" class="col-sm-3 text-right control-label col-form-label">Amount</label>
                    <div class="col-sm-6">
                        <input type="number" name="amount" class="form-control" id="amount"
                               value="{{ $premiums->total_collected }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="fine" class="col-sm-3 text-right control-label col-form-label">Fine</label>
                    <div class="col-sm-6">
                        <input type="number" name="fine" class="form-control" id="fine" value="{{ $premiums->penalty }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="fine" class="col-sm-3 text-right control-label col-form-label">Date</label>
                    <div class="col-sm-6">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="datepicker" class="form-control pull-right" id="datepicker"
                                   value="{{ $premiums->payment_date }}">
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>
    </div>



@endsection

