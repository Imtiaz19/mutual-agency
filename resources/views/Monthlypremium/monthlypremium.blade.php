@extends('layouts.master')
@section('title','Registered User')
@section('con')
    @include('layouts.error')
    @include('flash::message')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"> <b>Monthly Premium</b> </h3></div>
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('premium.store') }}" method="post">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3 text-right control-label col-form-label">Members Name</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="member">
                            @foreach($users as $user)
                                <option hidden disabled selected value> Select a member</option>
                                <option value="{{$user->id}}">{{$user->first_name.' '.$user->last_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="month" class="col-sm-3 text-right control-label col-form-label">Month of Payment</label>
                    <div class="col-sm-6">
                        <div id="sla-data-range" class="mrp-container nav navbar-nav">
                            <span class="mrp-icon"><i class="fa fa-calendar"></i></span>
                            <div class="mrp-monthdisplay" name="mop">
                                <span class="mrp-lowerMonth" id="mrp">Jul 2018</span>
                                <span class="mrp-to"> to </span>
                                <span class="mrp-upperMonth">Aug 2014</span>
                            </div>

                            
                        </div>
                        <input type="text"  id="mrp-upperDate"/>
                        <input type="text" id="mrp-lowerDate"/>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="amount" class="col-sm-3 text-right control-label col-form-label">Amount</label>
                    <div class="col-sm-6">
                        <input type="number" name="amount" class="form-control" id="amount">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="fine" class="col-sm-3 text-right control-label col-form-label">Fine</label>
                    <div class="col-sm-6">
                        <input type="number" name="fine" class="form-control" id="fine">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="fine" class="col-sm-3 text-right control-label col-form-label">Date</label>
                    <div class="col-sm-6">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="datepicker" class="form-control pull-right" id="datepicker">
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>
    </div>



@endsection

