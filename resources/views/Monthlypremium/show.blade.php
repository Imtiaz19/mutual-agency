@extends('layouts.master')
@section('title', 'Registered User')
@section('con')

    @include('layouts.error')
    @include('flash::message')
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><b>Monthly Premiums</b> </h3>
            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">

                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Month of Payment</th>
                    <th>Amount</th>
                    <th>Fine</th>
                    <th>payment Date</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @forelse($premiums as $premium)
                    <tr>
                        <td>{{ $premium->member_id }}</td>
                        <td>{{ $premium->month_of_payment }}</td>
                        <td>{{ $premium->total_collected }}</td>
                        <td>{{ $premium->penalty }}</td>
                        <td>{{ $premium->payment_date}}</td>
                        <td>
                            <a class="btn btn-raised btn-primary btn-sm"
                               href="{{ route('premium.edit',$premium->id) }}"><i class="fa fa-edit"
                                                                                  aria-hidden="true"></i></a>
                            <form action="{{route('premium.destroy',$premium->id)}}" id="delete-form-{{ $premium->id }}"
                                  method="post" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                            </form>
                            <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $premium->id }}').submit();
                                    }else{
                                    event.preventDefault();
                                    }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection