<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src={{ asset('dist/img/user2-160x160.jpg') }} class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Member Registration</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href={{ route('register.create') }}><i class="fa fa-circle-o"></i> Registration</a></li>
                    <li><a href={{ route('register.show') }}><i class="fa fa-circle-o"></i> Registered Member</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Role</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href={{ route('role.create' ) }}><i class="fa fa-circle-o"></i> Create Role</a></li>
                    <li><a href={{ route('role.index' ) }}><i class="fa fa-circle-o"></i> All roles</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Permission</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href={{ route('permission.create' ) }}><i class="fa fa-circle-o"></i> Create Permission</a>
                    </li>
                    <li><a href={{ route('permission.index' ) }}><i class="fa fa-circle-o"></i> All permission</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Monthly Premium</span>
                    <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href={{ route('premium.create' ) }}><i class="fa fa-circle-o"></i>Monthly Premium</a></li>
                    <li><a href={{ route('premium.index' ) }}><i class="fa fa-circle-o"></i> All Monthly Premium</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Investment</span>
                    <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href={{ route('investment.create' ) }}><i class="fa fa-circle-o"></i>Investment</a></li>
                    <li><a href={{ route('investment.index' ) }}><i class="fa fa-circle-o"></i> All Investment</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Loan</span>
                    <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href={{ route('loan.create' ) }}><i class="fa fa-circle-o"></i>Create Loan</a></li>
                    <li><a href={{ route('loan.index' ) }}><i class="fa fa-circle-o"></i> All Loans</a>
                       
                    </li>
                </ul>
            </li>

    </section>
    <!-- /.sidebar -->
</aside>