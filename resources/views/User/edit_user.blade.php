@extends('layouts.master')
@section('title', 'Edit Member')
@section('con')
    @include('flash::message')
    @include('layouts.error')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title">Edit Member</h3></div>
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('register.update',$users->id) }}" method="post">
                {{csrf_field()}}
                {{method_field('PATCH')}}
                <div class="form-group row">
                    <label for="firstname" class="col-sm-3 text-right control-label col-form-label">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="firstname" class="form-control" id="firstname"
                               value="{{$users->first_name}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lastname" class="col-sm-3 text-right control-label col-form-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="lastname" class="form-control" id="lastname"
                               value="{{$users->last_name}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-sm-3 text-right control-label col-form-label">User Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="username" class="form-control" id="username"
                               value="{{$users->username}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-sm-3 text-right control-label col-form-label">Mobile</label>
                    <div class="col-sm-9">
                        <input type="text" name="phone" class="form-control" id="phone" value="{{$users->phone}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                    <div class="col-sm-9">
                        <input type="text" name="email" class="form-control" id="email" value="{{$users->email}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-3 text-right control-label col-form-label">Address</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="address">{{$users->address}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="role" class="col-sm-3 text-right control-label col-form-label">Roles</label>
                    <div class="col-sm-9">
                        @foreach($roles as $role)
                            <input type="checkbox" {{in_array($role->id,$role_user)?"checked":""}}  id="roll"
                                   name="role[]" value="{{$role->id}}"> {{$role->name}} <br>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
