@extends('layouts.login')
@section('title','Create your password')
@section('header','Please enter your password')
@section('content')
    @include('layouts.error')
    @include('flash::message')
    <form action={{ route('success') }} method="post">
        {{csrf_field()}}
        <div class="form-group has-feedback">
            <input id="password" name="password" placeholder="Enter your password" type="password" class="form-control"
                   required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input name="password_confirmation" placeholder="Retype password" type="password" class="form-control"
                   required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        @foreach($users as $user)
            <input type="hidden" name="vtoken" value="{{$user->verification_token}}">
        @endforeach
        <div class="row">
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    Submit
                </button>
            </div>
        </div>
    </form>
@endsection