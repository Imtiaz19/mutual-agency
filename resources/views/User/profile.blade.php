@extends('layouts.master')
@section('title', 'User Registration')
@section('con')
    @include('layouts.error')
    @include('flash::message')
    <div class="box">
        <div class="box-header with-border"><h2
                    class="box-title"> {{ Auth::user()->first_name.' '.Auth::user()->last_name}} Profile </h2></div>
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('update_profile',Auth::user()->id)}}" method="post"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                
                @if(Auth::user()->photo)
                    <img src="/images/{{ Auth::user()->photo }}" class="rounded-circle" width="150" height="150"
                         alt="image" style=" float:left; margin-right:35px">
                @else
                    <img src={{ asset('images/default.jpg') }} class="rounded-circle" width="150" height="150"
                         alt="image"
                         style=" float:left; margin-right:35px">
                @endif

                <label>Update profile image</label>
                <div class="form-group row">
                    <input type="file" name="image">
                </div>

                <div class="pro" style="margin-top:50px">
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="fname" class="form-control" id="fname"
                                   value="{{ Auth::user()->first_name }} ">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="lname" class="form-control" id="lname"
                                   value="{{ Auth::user()->last_name }} ">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="username" class="col-sm-3 text-right control-label col-form-label">User Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" id="username" class="form-control"
                                   value="{{ Auth::user()->username }} ">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-sm-3 text-right control-label col-form-label">Mobile</label>
                        <div class="col-sm-9">
                            <input type="text" name="phone" class="form-control" id="phone"
                                   value="{{ Auth::user()->phone }} ">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" name="email" class="form-control" id="email"
                                   value="{{ Auth::user()->email }} ">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-sm-3 text-right control-label col-form-label">Address</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="address"
                                      id="address"> {{ Auth::user()->address }} </textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>

@endsection
