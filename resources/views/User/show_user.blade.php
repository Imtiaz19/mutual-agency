@extends('layouts.master')
@section('title','Registered User')
@section('con')
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <p>{{$error}}</p>
            </div>
        @endforeach
    @endif
    @include('flash::message')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Registered Member</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Username</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Email</th>
                    <th scope="col">Address</th>
                    <th scope="col">Action</th>
                </tr>
                <tbody>
                <?php $count = 1; ?>
                @forelse($users as $user)
                    <tr>
                        <th scope="row">{{ $count++ }}</th>
                        <td>{{$user->first_name}}</td>
                        <td>{{$user->last_name}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->address}}</td>
                        <td>
                            <a class="btn btn-raised btn-primary btn-sm"
                               href="{{ route('register.edit',$user->id) }}"><i class="fa fa-edit"
                                                                                aria-hidden="true"></i></a>
                            <form action="{{route('register.destroy',$user->id)}}" id="delete-form-{{ $user->id }}"
                                  method="post" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                            </form>
                            <button onclick="if(confirm('Are you Sure, You went to delete this?')){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $user->id }}').submit();
                                    }else{
                                    event.preventDefault();
                                    }" class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No Register User</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection