@extends('layouts.master')
@section('title', 'User Registration')
@section('con')

    @include('layouts.error')
    @include('flash::message')
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title"> <b>Member Registration</b> </h3></div>
        <div class="box-body">
            <form action="{{ route('register.store') }}" method="post">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="fname" class="form-control" id="fname" placeholder="First Name Here">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="lname" class="form-control" id="lname" placeholder="Last Name Here">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-sm-3 text-right control-label col-form-label">User Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="username" class="form-control" id="username"
                               placeholder="User Name Here">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-sm-3 text-right control-label col-form-label">Mobile</label>
                    <div class="col-sm-9">
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Mobile number">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                    <div class="col-sm-9">
                        <input type="text" name="email" class="form-control" id="email" placeholder="Email Here">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address" class="col-sm-3 text-right control-label col-form-label">Address</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="address"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="role" class="col-sm-3 text-right control-label col-form-label">Roles</label>
                    <div class="col-sm-9">
                        @foreach($roles as $role)
                            <input type="checkbox" id="roll" name="role[]" value="{{$role->id}}"> {{$role->name}} <br>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

