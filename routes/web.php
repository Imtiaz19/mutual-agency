<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', [
    'as' => 'admin.dashboard',
    'uses' => function () {
        return view('home');
    }
]);


Route::resource('register','Auth\RegisterController');
Route::resource('premium','PremiumController');
Route::get('show','Auth\RegisterController@showuser')->name('register.show');
Route::get('profile','Homecontroller@profile')->name('profile');
Route::post('update/{id}','Homecontroller@update_profile')->name('update_profile');

Route::get('/home','HomeController@index')->name('home');
Route::resource('permission','Permissioncontroller');
Route::resource('investment','Investmentcontroller');
Route::resource('role','Rolecontroller');
Route::resource('loan','LoanPersonController');
Route::get('/loan/{file}','LoanPersonController@edit')->name('pdf');
// verification route 
Route::get('varification/{verification_token}', 'Auth\RegisterController@createpass')->name('verify');
//activation route
Route::post('success', 'Auth\RegisterController@activeuser')->name('success');


