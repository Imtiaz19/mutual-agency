<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $fillable =[
        'user_id','user_type','investment_type','monthly_payment','interest_type','percentage_rate','issue_date','target_date_to_collect',
        'payment_due','last_payment_installment','created_by','updated_by'

    ];
    
}
