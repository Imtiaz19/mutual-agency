<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\VerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable 
{
    use Notifiable;use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','username','phone','email','address','photo','verification_token','password','created_by',
        'updated_by'
    ];
    public function verified()
    {
        return $this->varification_token === null;
    }
    public function sendVerificationEmail()
    {
        $this->notify(new VerifyEmail($this));
    }



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_by'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
