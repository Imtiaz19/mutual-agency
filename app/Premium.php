<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Premium extends Model
{
    protected $fillable=
    ['member_id','month_of_payment','total_collected','penalty','payment_date','created_by','updated_by'];      

public function member()
{
return $this->belongsTo(App\User::class,'member_id');
}
}