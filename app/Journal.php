<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Journal extends Model
{  use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $fillable=['amount','payment_id','payment_format','short_description','created_by','updated_by'];
    
   
    public function  loan()
    {
        return $this->hasOne(Loan::class,'payment_id');
    }
    public function  monthly()
    {
        return $this->hasOne(Monthly::class,'payment_id');
    }
  
    




}
