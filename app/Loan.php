<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable =['full_name','address','email','mobile','document_collected','loaner_photo','guarantor_name',
    'guarantor_phone','guarantor_photo','guarantor_email','document','loan_amount','loan_due','last_loan_installment',
     'issue_date','approved_proof','approved_date','last_full_payment_date','approved_by','created_by','updated_by','status'   ];
    
     public function  journal()
     {
         return $this->belongsTo(Loan::class);
     }
    }
