<?php

namespace App\Http\Controllers;
use App\User;
use App\Investment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class InvestmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $investments=Investment::all();
       return view('Investment.show',compact('investments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('Investment.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'member' => 'required',
            'user_type' => 'required',
            'investment_type' => 'required',
            'monthly_payment' => 'required',
            'interest_type' => 'required',
            'percentage_rate' => 'required',
            'issue_date' => 'required',
            'target_date_to_collect' => 'required',
            'payment_due' => 'required',
            'last_payment_installment' => 'required',
            
    ]);
        Investment::create([
        'user_id' => $request['member'],
        'user_type' => $request['user_type'],
        'investment_type' => $request['investment_type'],
        'monthly_payment' => $request['monthly_payment'],
        'interest_type' => $request['interest_type'],
        'percentage_rate' => $request['percentage_rate'],
        'issue_date' => date("Y-m-d ",strtotime($request['issue_date'])), 
        'target_date_to_collect' => date("Y-m-d",strtotime($request['target_date_to_collect'])),
        'payment_due' => $request['payment_due'],
        'last_payment_installment' => date("Y-m-d",strtotime($request['last_payment_installment'])),
        'created_by'=>Auth::user()->id,
        'updated_by'=>Auth::user()->id
        ]);
        flash('Created successfully')->important()->success();
        return redirect()->route('investment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Investment  $investment
     * @return \Illuminate\Http\Response
     */
    public function show(Investment $investment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Investment  $investment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $investments=Investment::find($id);
        $users = User::get();
        return View('Investment.edit',compact(['investments','users']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Investment  $investment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $investments= Investment::find($id);
        $investments->user_id =$request['member'];
        $investments->user_type = $request['user_type'];
        $investments->investment_type =$request['investment_type'];
        $investments->monthly_payment = $request['monthly_payment'];
        $investments->interest_type = $request['interest_type'];
        $investments->percentage_rate =$request['percentage_rate'];
        $investments->issue_date =date("Y-m-d ",strtotime($request['issue_date'])); 
        $investments->target_date_to_collect =date("Y-m-d",strtotime($request['target_date_to_collect']));
        $investments-> payment_due =$request['payment_due'];
        $investments-> last_payment_installment =date("Y-m-d",strtotime($request['last_payment_installment']));
        $investments-> created_by=Auth::user()->id;
        $investments->updated_by=Auth::user()->id;
        $investments->save();
        flash(' Updated successfully')->important()->success();
        return redirect()->route('investment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Investment  $investment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Investment::find($id)->delete();
       flash('Deleted successfully')->important()->success();
        return redirect()->route('investment.index');
    }
}
