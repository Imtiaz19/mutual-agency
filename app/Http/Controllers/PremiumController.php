<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Monthly;
use App\Journal;


class PremiumController extends Controller
{
    public function index()
    {
        $premiums=Monthly::all();
        return view('Monthlypremium.show',compact('premiums'));
    }
    public function create()
    {
        $users = User::all();
        return view('Monthlypremium.monthlypremium', compact('users'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'member' => 'required',
            'amount' => 'required',
            'fine' => 'required',
            'datepicker' => 'required',
           
        ]);
        $monthly=Monthly::create([
        'member_id' => $request['member'],
        'month_of_payment' => 'January',
        'total_collected' => $request['amount'],
        'penalty'=>$request['fine'],
        'payment_date'=> date("y-m-d",strtotime($request['datepicker'])),
        'created_by'=>Auth::user()->id,
        'updated_by'=>Auth::user()->id
        ]);
       
        Journal::create([
        'amount'=>$request['amount'] + $request['fine'],
        'payment_id'=>$monthly->id,
        'payment_format'=>'monthly',
        'short_description'=>'fine',
        'created_by'=>Auth::user()->id,
        'updated_by'=>Auth::user()->id,

        ]);

        flash('Created successfully')->important()->success();
        return redirect()->route('premium.index');
    }
    public function edit ($id)
    {
        $premiums=Monthly::find($id);
        $users = User::get();
        return View('Monthlypremium.edit',compact(['premiums','users']));

    }
    public function update(Request $request, $id)
    {
        $premium=Monthly::find($id);
        $premium->member_id=$request['member'];
        $premium->month_of_payment='February';
        $premium->total_collected=$request->amount;
        $premium->penalty=$request->fine;
        $premium->payment_date=date("Y-m-d ",strtotime($request['datepicker'])); 
        $premium->created_by=Auth::user()->id;
        $premium->updated_by=Auth::user()->id;
        $premium->save();
        flash(' Updated successfully')->important()->success();
        return redirect()->route('premium.index');
    }
    public function destroy($id)
    {
        Monthly::find($id)->delete();
        flash('Deleted successfully')->important()->success();
        return redirect()->route('premium.index');

    }

}
