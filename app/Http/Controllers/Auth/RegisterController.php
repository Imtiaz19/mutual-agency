<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class RegisterController extends Controller
{


    use RegistersUsers;
    

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function index()
    {
        //return redirect()->route('login');
    }


//  show register users
    public function showuser()
    {
        $users = User::all();
        return view('user.show_user', compact('users'));
    }


//  create user register controlelr
    public function create()

    {   $roles = Role::all();
        return view('register.create',compact('roles'));
    }



//  password create controller
    public function createpass()
    {
        $users = User::all();
        return view('user.create_password', compact('users'));
    }



//  register user and give role to the user
    public function store (Request $request)
    {    /*validation*/
      $rules = [
        'fname' => 'required',
        'lname' => 'required',
        'username' => 'required',
        'phone' => 'required|unique:users',
        'email' => 'required|unique:users|email',
        'address' => 'required',
    ];
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }

    $user = User::create([
        'first_name' => $request['fname'],
        'last_name' => $request['lname'],
        'username' => $request['username'],
        'phone' => $request['phone'],
        'email' => $request['email'],
        'address' => $request['address'],
        'created_by' => Auth::user()->id,
        'updated_by' => Auth::user()->id,
        'verification_token' => md5(str_random(10)),

    ]);
    if( $request->has('role')){ 
    foreach ($request->input('role') as $key => $value) {
        $user->attachRole($value);
    }
    }
    //  sent email and redirect
    $user->sendVerificationEmail();
    flash('User created successfully and verification mail has been sent')->important()->success();
    return redirect()->route('register.show');
    
    


    


}

//  edit user info
public function edit($id)
{
    /*get info by user id */
    $users = User::find($id);
    $roles = Role::get();
    $role_user = DB::table("role_user")
    ->where("role_user.user_id", $id)
    ->pluck('role_user.role_id')->toArray();
    return view('user.edit_user', compact(['users', 'roles', 'role_user']));
}
public function update(Request $request, $id)
{
    /*validation*/
    $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'username' => 'required',
        'phone' => 'required|',
        'email' => 'required|email',
        'address' => 'required',
    ];
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }
    //update user info
    $user = User::find($id);
    $user->first_name = $request->firstname;
    $user->last_name = $request->lastname;
    $user->username = $request->username;
    $user->phone = $request->phone;
    $user->email = $request->email;
    $user->address = $request->address;
    $user->save();
    DB::table('role_user')->where('user_id', $id)->delete();

    if( $request->has('role')){ 
    foreach ($request->input('role') as $key => $value) {
        $user->attachRole($value);
    }
}
    /*Redirect*/
    flash('User Updated successfully')->important()->success();
    return redirect()->route('register.show');

    
}


//  delete user from db
public function destroy($id)
{
   User::find($id)->delete();
    flash('User Deleted successfully')->important()->success();
    return redirect()->route('register.show');
    
}



//  active user
public function activeuser(Request $request)
{
    $rules = [
        'password' => 'min:6|required', 
        'password_confirmation' => 'same:password',
    ];
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }
    $cheak = User::whereverification_token($request['vtoken'])->first();
    if (!is_null($cheak)) {
        $user=User::find($cheak->id);
        if ($user->is_active==1) {
            flash('Your account already activated')->important()->error();
            return redirect()->back();
        }
        $user->password = bcrypt($request['password']);
        $user->is_active='1';
        $user->verification_token=null;
        $user->save();
        flash('Succes! Your account has been activated')->important()->success();
        return redirect()->back();
    }
    flash('Sorry! Wrong verification token')->error();
    return redirect()->back();
}


}








