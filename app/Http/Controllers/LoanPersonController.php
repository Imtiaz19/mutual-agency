<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Loan;
use App\Journal;
use Illuminate\Support\Facades\Storage;

class LoanPersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loans=Loan::all();
        return view('Loan.index',compact('loans'));
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Loan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'full_name'=>'required',
            'address'=>'required',
            'email'=>'required|email',
            'mobile'=>'required',
            'document_collected'=>'required|mimes:jpeg,png,jpg,doc,pdf,docx|max:2024',
            'loaner_photo'=>'required|mimes:jpeg,png,jpg|max:2024',
            'guarantor_name'=>'required',
            'guarantor_phone'=>'required',
            'guarantor_photo'=>'required|mimes:jpeg,png,jpg|max:2024',
            'guarantor_email'=>'required',
            'document'=>'required|mimes:jpeg,png,jpg,doc,pdf,docx|max:2024',
            'loan_amount'=>'required',
            'loan_due'=>'required',
            'last_loan_installment'=>'required',
            'issue_date'=>'required',
            'approved_date'=>'required',
            'approved_proof'=>'required|mimes:jpeg,png,jpg,doc,pdf,docx|max:2024',
            'last_full_payment_date'=>'required',

        ]);
        if( $request->hasFile('document','loaner_photo','document_collected','guarantor_photo','approved_proof')){ 
            $loanerPhotoFile = $request->file('loaner_photo');
            $documentFile = $request->file('document');
            $documentCollectedFile = $request->file('document_collected');
            $guarantorPhotoFile = $request->file('guarantor_photo');
            $approvedProoFile = $request->file('approved_proof');
            $loaner_photo_name=$loanerPhotoFile->getClientOriginalName();
            $document_name=$documentFile->getClientOriginalName();
            $document_collected_name=$documentCollectedFile->getClientOriginalName();
            $gurantar_photo_name=$guarantorPhotoFile->getClientOriginalName();
            $approved_proof_name=$approvedProoFile->getClientOriginalName();
            $loanerPhotoFile->move(public_path('images'),$loaner_photo_name  );
            $documentFile->move(public_path('files'),$document_name  );
            $documentCollectedFile->move(public_path('files'),$document_collected_name  );
            $guarantorPhotoFile->move(public_path('images'),$gurantar_photo_name  );
            $approvedProoFile->move(public_path('files'),$approved_proof_name  );
        }
        
       
        $loan= New Loan;
        $loan->full_name=$request->full_name;
        $loan->address=$request->address;
        $loan->email=$request->email;
        $loan->mobile=$request->mobile;
        $loan->guarantor_name=$request->guarantor_name;
        $loan->guarantor_phone=$request->guarantor_phone;
        $loan->guarantor_name=$request->guarantor_name;
        $loan->guarantor_email=$request->guarantor_email;
        $loan->loan_amount=$request->loan_amount;
        $loan->loan_due=$request->loan_due;
        $loan->last_loan_installment=$request->last_loan_installment;
        $loan->last_full_payment_date=date("Y-m-d ",strtotime($request['last_full_payment_date']));
        $loan->issue_date=date("Y-m-d ",strtotime($request['issue_date']));
        $loan->approved_by=Auth::user()->id;
        $loan->approved_date=date("Y-m-d ",strtotime($request['approved_date']));
        $loan->created_by=Auth::user()->id;
        $loan->updated_by=Auth::user()->id;
        $loan->document_collected=$document_collected_name;
        $loan->loaner_photo=$loaner_photo_name;
        $loan->guarantor_photo=$gurantar_photo_name;
        $loan->document=$document_name;
        $loan->approved_proof=$approved_proof_name;
        if( $request->hasFile('approved_proof')){
            $loan->status='1';
         }
        $loan->save();
        
        $journal= New Journal();
        $journal->amount=$request->loan_amount;
        $journal->payment_id=$journal->loan->id;
        $journal->payment_format=loan;
        $journal->short_description=loan;
        $journal->created_by=Auth::user()->id;
        $journal->updated_by=Auth::user()->id;
            

        flash('Succes! Your account has been updated')->important()->success();
        return redirect()->route('loan.index');
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loans=Loan::find($id);
        return view('Loan.show',compact('loans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $loans=Loan::find($id);
        //return response()->download(storage_path('app/' . $file->location));
        return View('Loan.edit',compact(['loans']));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $loan=Loan::find($id);
        $loan->full_name=$request->full_name;
        $loan->address=$request->address;
        $loan->email=$request->email;
        $loan->mobile=$request->mobile;
        $loan->guarantor_name=$request->guarantor_name;
        $loan->guarantor_phone=$request->guarantor_phone;
        $loan->guarantor_name=$request->guarantor_name;
        $loan->guarantor_email=$request->guarantor_email;
        $loan->loan_amount=$request->loan_amount;
        $loan->loan_due=$request->loan_due;
        $loan->last_loan_installment=$request->last_loan_installment;
        $loan->last_full_payment_date=date("Y-m-d ",strtotime($request['last_full_payment_date']));
        $loan->issue_date=date("Y-m-d ",strtotime($request['issue_date']));
        $loan->approved_by=Auth::user()->id;
        $loan->approved_date=date("Y-m-d ",strtotime($request['approved_date']));
        $loan->created_by=Auth::user()->id;
        $loan->updated_by=Auth::user()->id;

        if( $request->hasFile('loaner_photo'))
        { 
            $loanerPhotoFile = $request->file('loaner_photo');
            $loaner_photo_name=$loanerPhotoFile->getClientOriginalName();
            $loanerPhotoFile->move(public_path('images'),$loaner_photo_name  );
            $loan->loaner_photo=$loaner_photo_name;
        }
        if( $request->hasFile('document'))
        { 
            $documentFile = $request->file('document');
            $document_name=$documentFile->getClientOriginalName();
            $documentFile->move(public_path('files'),$document_name);
            $loan->document=$document_name;
        }
        if( $request->hasFile('document_collected'))
        {
            $documentCollectedFile = $request->file('document_collected');
            $document_collected_name=$documentCollectedFile->getClientOriginalName();
            $documentCollectedFile->move(public_path('files'),$document_collected_name  );
            $loan->document_collected=$document_collected_name;
        }
        if( $request->hasFile('guarantor_photo'))
        {
            $guarantorPhotoFile = $request->file('guarantor_photo');
            $gurantar_photo_name=$guarantorPhotoFile->getClientOriginalName();
            $guarantorPhotoFile->move(public_path('images'),$gurantar_photo_name  );
            $loan->guarantor_photo=$gurantar_photo_name;
        }
        if( $request->hasFile('approved_proof'))
        {
            $approvedProoFile = $request->file('approved_proof');
            $approved_proof_name=$approvedProoFile->getClientOriginalName();
            $approvedProoFile->move(public_path('files'),$approved_proof_name  );
             $loan->approved_proof=$approved_proof_name;
        }           
        if( $request->hasFile('approved_proof'))
        {
            $loan->status='1';
        }

        if( $request->hasFile('approved_proof')){
            $loan->status='1';
         }
        $loan->save();

        flash('Succes! Your account has been updated')->important()->success();
        return redirect()->route('loan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Loan::find($id)->delete();
       flash('Deleted successfully')->important()->success();
        return redirect()->route('loan.index');
    }
}
