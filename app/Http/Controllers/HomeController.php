<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function profile()
    {
        
        return view('user.profile');
    }
    public function update_profile(Request $request, $id)
    {
        $rules = [
            'fname' => 'required',
            'lname' => 'required',
            'username' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:1024',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
         //update user info
         $user=User::find($id);
         $user->first_name = $request->fname;
         $user->last_name = $request->lname;
         $user->phone = $request->phone;
         $user->email = $request->email;
         $user->address = $request->address;
        if( $request->hasFile('image')){ 
        $image = $request->file('image');
        $new_name= time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $new_name);
        $user->photo=$new_name;
         }
         $user->save();
       
         
         /*Redirect*/
        flash('Succes! Your account has been updated')->important()->success();
         return redirect()->back();
         
    }
}

