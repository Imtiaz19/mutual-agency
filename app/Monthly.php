<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monthly extends Model
{
    protected $fillable=
    ['member_id','month_of_payment','total_collected','penalty','payment_date','created_by','updated_by'];   
   
    public function  journal()
    
    {
        return $this->belongsTo(Journal::class);
    }
}
