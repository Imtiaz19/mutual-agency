<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions=[
                [
                    'name' => 'Role-read',
                    'display_name' => 'Display role Listing',
                    'description' => 'See only Listing Of role'
                ]
            ];
        foreach ($permissions as $key=>$value){
            Permission::create($value);
        }
    }
}
