<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Roles =[
            [
                'name' => 'member',
                'display_name' => 'Member',
                'description' => 'All type of user'
            ],
            [
                'name' => 'admin',
                'display_name' => 'Admin',
                'description' => ''
            ],
            [
                'name' => 'accountant',
                'display_name' => 'Accountant',
                'description' => ''
            ],
            [
                'name' => 'super-admin',
                'display_name' => 'Super Admin',
                'description' => ''
            ]
        ];

        foreach ($Roles as $key=>$value){
            Role::create($value);
        }
    }
}
