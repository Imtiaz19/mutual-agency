<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name'=>'imtiaz',
            'last_name'=>'ahmed',
            'username'=>'imtiaz',
            'phone'=>'0168',
            'address'=>'dhaka',
            'created_by'=>1,
            'updated_by'=>1,
            'email'=>'imtiaz@gmail.com',
            'password'=> Hash::make('imtiaz01')
        ]);
    }
}
