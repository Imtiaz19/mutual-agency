<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->increments('id');
            $table->double('amount');
            $table->integer('payment_id')->unsigned();
            $table->enum('payment_format', ['monthly','investment','loan','income','expense','others']);
            $table->string('short_description');
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->dateTime('deleted_at');
            $table->timestamps();

           $table->foreign('payment_id')->references('id')->on('monthlies')
                ->onUpdate('cascade')->onDelete('cascade');
        //    $table->foreign('payment_id')->references('id')->on('investments')
        //        ->onUpdate('cascade')->onDelete('cascade');
        //    $table->foreign('payment_id')->references('id')->on('loans')
        //        ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('created_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
