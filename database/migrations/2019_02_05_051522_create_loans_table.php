<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('address');
            $table->string('email',30);
            $table->string('mobile',  20);
            $table->string('document_collected');
            $table->string('loaner_photo', 50);
            $table->string('guarantor_name', 50);
            $table->string('guarantor_phone', 20);
            $table->string('guarantor_photo', 50);
            $table->string('guarantor_email', 30);
            $table->string('document', 50);
            $table->double('loan_amount');
            $table->double('loan_due');
            $table->double('last_loan_installment');
            $table->dateTime('issue_date');
            $table->integer('approved_by')->unsigned();
            $table->dateTime('approved_date')->nullable();
            $table->string('approved_proof', 50);
            $table->dateTime('last_full_payment_date');
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->boolean('status')->default(1);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('approved_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
